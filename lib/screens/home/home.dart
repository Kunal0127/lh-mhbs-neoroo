// import 'dart:html';

import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<String> _babyList = ["Mila", "Joe", "Henry"];
  final vitalsTitle = ["Temprature", "Breating Rate", "Heart Rate"];
  final vitalsValue = ["97.00", "97.00", "97.00"];
  final vitalsColor = [Colors.green, Colors.red, Colors.blue];
  final vitalsIcon = [Icons.thermostat, Icons.boy_rounded, Icons.favorite];
  //  String _selectedBaby;
  var _selectedBaby;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Color(0xFF6E78F7),
          title: Text('Welcome back!'),
          actions: [
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.notifications_rounded),
            )
          ],
          toolbarHeight: 100,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(bottom: Radius.circular(20))),
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 16),
                Text(
                  'About Us',
                  style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
                ),
                SizedBox(height: 16),
                AboutUsCard(),
                SizedBox(height: 22),
                _babyHeader(),
                SizedBox(height: 16),
                Text(
                  "Vitals",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 22),
                SizedBox(
                  height: 110,
                  child: ListView.separated(
                    physics: AlwaysScrollableScrollPhysics(),
                    itemCount: 3,
                    scrollDirection: Axis.horizontal,
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(width: 16);
                    },
                    itemBuilder: (BuildContext context, int index) {
                      return VitalsWidget(
                        title: vitalsTitle[index],
                        value: vitalsValue[index],
                        icon: vitalsIcon[index],
                        colorValue: vitalsColor[index],
                      );
                    },
                  ),
                ),
                SizedBox(height: 16),
                Text(
                  "Skin-to-Skin",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 16),

//////////////////////////////////////////////////////////

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    StsWigdet(
                      icon: Icons.access_time_outlined,
                      title: "STS Time",
                      value: "20.09 min",
                    ),
                    StsWigdet(
                      icon: Icons.access_time_outlined,
                      title: "STS this week",
                      value: "30 hr",
                    ),
                  ],
                ),
//////////////////////////////////////////////////////////////
                SizedBox(
                  height: 16,
                ),
                Text(
                  "New Training Module",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 16,
                ),
///////////////////////////////////////////////////////////
                TrainingModuleCard(
                    title: "Carrying your baby skin-to-skin",
                    details:
                        "NeoRoo is an application connected to sensor-enabled swaddling device.",
                    image: Image.asset(
                      'assets/skin-to-skin newborn.jpg',
                      fit: BoxFit.cover,
                    )),
                SizedBox(
                  height: 16,
                ),
///////////////////////////////////////////////////////////
                TrainingModuleCard(
                    title: "Carrying your baby skin-to-skin",
                    details:
                        "NeoRoo is an application connected to sensor-enabled swaddling device.",
                    image: Image.asset(
                      'assets/skin-to-skin newborn.jpg',
                      fit: BoxFit.cover,
                    )),
                SizedBox(
                  height: 16,
                ),
///////////////////////////////////////////////////////////
              ],
            ),
          ),
        ),
      ),
    );
  }

  Row _babyHeader() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          _selectedBaby != null
              ? "Baby $_selectedBaby"
              : "Baby ${_babyList[0]}",
          style: TextStyle(
              color: Color(0xFF6E78F7),
              fontSize: 26,
              fontWeight: FontWeight.bold),
        ),
        Container(
            width: 100,
            decoration: BoxDecoration(
                // color: Colors.white,
                borderRadius: BorderRadius.circular(16)),
            child: DropdownButtonHideUnderline(
              child: DropdownButton<String>(
                isExpanded: true,
                hint: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8.0),
                  child: Text('Select Baby'),
                ),
                value: _selectedBaby,
                items: _babyList.map((baby) {
                  return DropdownMenuItem(
                    child: Text(baby),
                    value: baby,
                  );
                }).toList(),
                onChanged: (newValue) {
                  setState(() {
                    _selectedBaby = newValue;
                  });
                },
              ),
            )),
      ],
    );
  }
}

class StsWigdet extends StatelessWidget {
  final IconData icon;
  final String title;
  final String value;

  const StsWigdet({
    super.key,
    required this.icon,
    required this.title,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 64,
      width: 150,
      child: PhysicalModel(
        color: Colors.white,
        elevation: 5,
        shadowColor: Colors.black,
        borderRadius: BorderRadius.circular(12),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(8),
                child: Container(
                  height: 40,
                  width: 40,
                  color: Color(0xFF6E78F7),
                  child: Center(
                      child: Icon(
                    icon,
                    color: Colors.white,
                  )),
                ),
              ),
              SizedBox(
                width: 8,
              ),
              Expanded(
                child: Column(
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      title,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 10,
                          color: Colors.grey),
                    ),
                    Text(value),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class VitalsWidget extends StatelessWidget {
  const VitalsWidget({
    super.key,
    required this.title,
    required this.value,
    required this.icon,
    this.colorValue = Colors.red,
  });

  final String title;
  final String value;
  final IconData icon;
  final Color? colorValue;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: 75,
          width: 150,
          margin: const EdgeInsets.only(top: 20),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, 1),
                    blurRadius: 2.5,
                    color: Colors.grey.shade400)
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const SizedBox(height: 20),
              //TODO: fix this spacing issue
              Text(
                title,
                style: TextStyle(fontWeight: FontWeight.normal),
              ),
              Text(
                '$value \u2109',
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: colorValue,
                    fontSize: 16),
              ),
            ],
          ),
        ),
        Positioned(
            top: 0,
            child: CircleAvatar(
              radius: 25,
              backgroundColor: Color(0xFF6E78F7),
              child: Icon(
                icon,
                color: Colors.white,
              ),
            ))
      ],
    );
  }
}

class AboutUsCard extends StatelessWidget {
  const AboutUsCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return PhysicalModel(
      color: Colors.white,
      elevation: 5,
      shadowColor: Colors.black,
      borderRadius: BorderRadius.circular(20),
      child: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
            child: Image.asset(
              "assets/aboutus_img.png",
            ),
          ),
          // SizedBox(
          //   height: 16,
          // ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              "Neoroo is an application connected to sensor-enabled swaddling device which helps to support the adoption and uptake of skin-to-skin care among adult-baby dyadsand automatically collects key vital signs inforation from newborns(for example,heart rate, breathing rate and pattern, oxygen Saturation), within the contex of the united States health care system.",
              style: TextStyle(
                fontWeight: FontWeight.w400,
              ),
              textAlign: TextAlign.justify,
            ),
          )
        ],
      ),
    );
  }
}

class TrainingModuleCard extends StatefulWidget {
  const TrainingModuleCard({
    super.key,
    required this.title,
    required this.details,
    required this.image,
  });
  final String title;
  final String details;
  final Image image;

  @override
  State<TrainingModuleCard> createState() => _TrainingModuleCardState();
}

class _TrainingModuleCardState extends State<TrainingModuleCard> {
  bool isDownloaded = false;
  bool isSaved = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 104,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(16),
          color: Colors.white,
          border: Border.all(color: Colors.grey.shade200, width: 1.2)),
      child: Row(
        children: [
          Stack(
            alignment: Alignment.center,
            children: [
              SizedBox(
                height: 104,
                width: 125,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: widget.image,
                ),
              ),
              InkWell(
                onTap: () {},
                child: CircleAvatar(
                    radius: 28,
                    backgroundColor: Colors.black45.withOpacity(0.65),
                    child: Icon(
                      Icons.play_arrow_rounded,
                      size: 48,
                      color: Colors.white,
                    )),
              )
            ],
          ),
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: Text(
                          widget.title,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                      Icon(
                        Icons.keyboard_arrow_right_rounded,
                        color: Colors.grey.shade500,
                      )
                    ],
                  ),
                  const SizedBox(height: 2),
                  Expanded(
                      child: Text(
                    widget.details,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      //download
                      GestureDetector(
                        onTap: () => setState(() {
                          isDownloaded = !isDownloaded;
                        }),
                        child: isDownloaded == false
                            ? Icon(Icons.save_alt_rounded,
                                color: Colors.grey.shade500)
                            : Icon(Icons.check_circle_outline_rounded,
                                color: Colors.green),
                      ),
                      const SizedBox(width: 8),

                      //bookmark
                      GestureDetector(
                        onTap: () => setState(() {
                          isSaved = !isSaved;
                        }),
                        child: isSaved == false
                            ? Icon(Icons.bookmark, color: Colors.grey.shade500)
                            : Icon(Icons.bookmark, color: Colors.black),
                      ),
                      const SizedBox(width: 8),
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
